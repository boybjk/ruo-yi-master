package com.ruoyi.web.controller.system;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.system.dto.BalanceDto;
import com.ruoyi.system.service.JkBalanceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
    @RequestMapping("/balance")
@Api(value = "余额信息", tags = "余额信息")
public class JkBalanceController extends BaseController {

    @Autowired
    private JkBalanceService jkBalanceService;

    @GetMapping("/list")
    @ApiOperation(value = "分页获取余额信息")
    @ResponseBody
    TableDataInfo getJkBalanceList(BalanceDto balanceDto) {
        startPage();
        List<BalanceDto> jkBalanceList = jkBalanceService.getJkBalanceList(balanceDto);
        return getDataTable(jkBalanceList);
    }


    /**
     * 测试
     * @return
     */
    @GetMapping(value = "/test")
    @ResponseBody
    public String listTest() {
        BalanceDto balanceDto=null;
        List<BalanceDto> list = jkBalanceService.getJkBalanceList(balanceDto);
        list.forEach(System.out::println);
        return "success";
    }



}
