package com.ruoyi.web.controller.system;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.system.dto.BonusDto;
import com.ruoyi.system.service.JkRewardLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.List;

@Controller
@RequestMapping("/reward/log")
@Api(value = "奖金发放记录管理", tags = "奖金发放记录管理")
public class JkRewardLogController extends BaseController {

    @Autowired
    private JkRewardLogService jkRewardLogService;

    @GetMapping(value = "/list")
    @ApiOperation(value = "分页奖金发放记录")
    @ResponseBody
    public TableDataInfo getJkRewardLogResultPage(@ModelAttribute BonusDto bonusDto) {
        startPage();  // 此方法配合前端完成自动分页
        List<BonusDto> list = jkRewardLogService.getJkRewardLogResultPage(bonusDto);
        return getDataTable(list);
    }

    @PostMapping(value = "/add")
    @ApiOperation(value = "新增奖金发放记录")
    public int addJkRewardLog(BonusDto bonusDto) {
        return jkRewardLogService.addJkRewardLog(bonusDto);
    }

    @GetMapping(value = "/del/{id}")
    @ApiOperation(value = "删除奖金发放记录")
    public int deleteJkRewardLogById(@PathVariable String id) {
        return jkRewardLogService.deleteJkRewardLogById(id);
    }



    /**
     * 新增奖金发放test
     */
    @GetMapping(value = "/test")
    @ResponseBody
    public String test() {
        //发奖金
        BonusDto bonusDto = new BonusDto();
        bonusDto.setEmpNo("1f6c13c7-7f09-402a-9919-5b38648e1617");
        bonusDto.setCompanyNo("21eaf7c8-197f-45ac-b820-832955e8b87c");
        bonusDto.setType(1);
        bonusDto.setBonus(BigDecimal.valueOf(1000.00));
        jkRewardLogService.addJkRewardLog(bonusDto);

        //取奖金
        BonusDto bonusDto2 = new BonusDto();
        bonusDto2.setEmpNo("1f6c13c7-7f09-402a-9919-5b38648e1617");
        bonusDto2.setCompanyNo("21eaf7c8-197f-45ac-b820-832955e8b87c");
        bonusDto2.setType(0);
        bonusDto2.setBonus(BigDecimal.valueOf(500.00));
        jkRewardLogService.addJkRewardLog(bonusDto2);

        return "success";
    }

    /**
     * 测试分页查询
     * @return
     */
    @GetMapping(value = "/list/test")
    @ResponseBody
    public String listTest() {
        BonusDto bonus = null;
        List<BonusDto> list = jkRewardLogService.getJkRewardLogResultPage(bonus);
        list.forEach(System.out::println);
        return "success";
    }


}
