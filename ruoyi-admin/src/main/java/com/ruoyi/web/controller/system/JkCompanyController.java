package com.ruoyi.web.controller.system;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.system.domain.JkCompanyEntity;
import com.ruoyi.system.service.JkCompanyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Set;

@Controller
@RequestMapping("/company")
@Api(value = "公司信息管理", tags = "公司信息管理")
public class JkCompanyController extends BaseController {

    @Autowired
    private JkCompanyService jkCompanyService;

    @GetMapping("/list")
    @ApiOperation(value = "获取所有公司信息")
    Set<JkCompanyEntity> getAllCompany() {
        return jkCompanyService.getAllCompany();
    }

    @GetMapping("/add")
    @ApiOperation(value = "新增公司信息")
    public int addJkCompany(JkCompanyEntity jkCompanyEntity) {
        return jkCompanyService.addJkCompany(jkCompanyEntity);
    }

    /**
     * 添加数据用于测试
     */
    @GetMapping("/test")
    public void test() {
        JkCompanyEntity entity = new JkCompanyEntity();
        entity.setCompanyName("软件公司");
        jkCompanyService.addJkCompany(entity);
    }
}
