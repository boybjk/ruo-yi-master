package com.ruoyi.web.controller.system;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.system.domain.JkCompanyEntity;
import com.ruoyi.system.domain.JkEmpEntity;
import com.ruoyi.system.service.JkCompanyService;
import com.ruoyi.system.service.JkEmpService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Set;

@Controller
@RequestMapping("/emp")
@Api(value = "员工信息管理", tags = "员工信息管理")
public class JkEmpController extends BaseController {

    @Autowired
    private JkEmpService jkEmpService;

    @Autowired
    private JkCompanyService jkCompanyService;

    @GetMapping(value = "/all")
    @ApiOperation(value = "获取所有员工信息")
    public Set<JkEmpEntity> getAllEmp() {
        return jkEmpService.getAllEmp();
    }

    @PostMapping(value = "/add")
    @ApiOperation(value = "新增员工信息")
    int addJkEmp(JkEmpEntity jkEmpEntity) {
        return jkEmpService.addJkEmp(jkEmpEntity);
    }


    /**
     * 新增测试
     */
    @GetMapping(value = "/add/test")
    public void test() {
        JkCompanyEntity compnay = jkCompanyService.getJkEmpByName("软件公司");
        String id = compnay.getId();
        JkEmpEntity entity = new JkEmpEntity();
        entity.setDepName("测试部门");
        entity.setCompanyId(id);
        entity.setEmpName("刘亦菲");
        jkEmpService.addJkEmp(entity);

        JkEmpEntity entity2 = new JkEmpEntity();
        entity2.setDepName("产品部门");
        entity2.setCompanyId(id);
        entity2.setEmpName("刘一刀");
        jkEmpService.addJkEmp(entity2);

        JkEmpEntity entity3 = new JkEmpEntity();
        entity3.setDepName("开发部门");
        entity3.setCompanyId(id);
        entity3.setEmpName("刘德华");
        jkEmpService.addJkEmp(entity3);

    }

}
