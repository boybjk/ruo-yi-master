package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.uuid.UUID;
import com.ruoyi.system.domain.JkCompanyEntity;
import com.ruoyi.system.domain.JkEmpEntity;
import com.ruoyi.system.mapper.JkCompanyMapper;
import com.ruoyi.system.service.JkCompanyService;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class JkCompanyServiceImpl implements JkCompanyService {

    @Autowired
    private JkCompanyMapper jkCompanyMapper;

    @Override
    public Set<JkCompanyEntity> getAllCompany() {
        return jkCompanyMapper.selectAll();
    }

    @Override
    public int addJkCompany(JkCompanyEntity jkCompanyEntity) {
        Validate.notNull(jkCompanyEntity.getCompanyName(),"jkCompanyEntity不能为空");
        jkCompanyEntity.setId(UUID.randomUUID().toString());
        return jkCompanyMapper.insert(jkCompanyEntity);
    }

    @Override
    public JkCompanyEntity getJkEmpByName(String name) {
        Validate.notNull(name, "name不能为空");
        return jkCompanyMapper.selectByName(name);
    }
}
