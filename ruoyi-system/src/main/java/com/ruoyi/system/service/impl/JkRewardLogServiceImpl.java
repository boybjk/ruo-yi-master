package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.exception.base.BaseException;
import com.ruoyi.system.domain.JkBalanceEntity;
import com.ruoyi.system.domain.JkEmpEntity;
import com.ruoyi.system.domain.JkRewardLogEntity;
import com.ruoyi.system.dto.BonusDto;
import com.ruoyi.system.mapper.JkBalanceMapper;
import com.ruoyi.system.mapper.JkCompanyMapper;
import com.ruoyi.system.mapper.JkEmpMapper;
import com.ruoyi.system.mapper.JkRewardLogMapper;
import com.ruoyi.system.service.JkRewardLogService;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service
public class JkRewardLogServiceImpl implements JkRewardLogService {

    @Autowired
    private JkRewardLogMapper jkRewardLogMapper;
    @Autowired
    private JkEmpMapper jkEmpMapper;
    @Autowired
    private JkCompanyMapper jkCompanyMapper;
    @Autowired
    private JkBalanceMapper jkBalanceMapper;

    @Override
    public List<BonusDto> getJkRewardLogResultPage(BonusDto bonusPage) {
        List<JkRewardLogEntity> JkRewardlist=jkRewardLogMapper.selectJkRewardLogListPage(bonusPage);
        if (null==JkRewardlist||JkRewardlist.size()==0){
            return new ArrayList<>();
        }
        List<BonusDto> bonusDtoList=new ArrayList<>();
        for (JkRewardLogEntity entity : JkRewardlist) {
            BonusDto bonusDto = new BonusDto();
            String empId = entity.getEmpId();
            String companyId = entity.getCompanyId();
            JkEmpEntity jkEmpEntity = jkEmpMapper.selectById(empId);
            if (null==jkEmpEntity){
                throw new BaseException("未查询到员工信息");
            }
            String companyName=jkCompanyMapper.selectById(companyId);
            if (null==companyName||"".equals(companyName)){
                throw new BaseException("未查询到公司信息");
            }
            bonusDto.setEmpName(jkEmpEntity.getEmpName());
            bonusDto.setCompany(companyName);
            bonusDto.setBonus(entity.getMoney());
            bonusDto.setType(entity.getType());
            bonusDto.setDescription(entity.getDescription());
            bonusDto.setEmpNo(empId);
            bonusDto.setCompanyNo(companyId);
            bonusDtoList.add(bonusDto);
        }
        return bonusDtoList;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int addJkRewardLog(BonusDto bonusDto) {
        String companyNo = bonusDto.getCompanyNo();
        Validate.notNull(companyNo,"公司编号不能为空");
        String empNo = bonusDto.getEmpNo();
        Validate.notNull(empNo,"员工编号不能为空");
        Integer type = bonusDto.getType();
        Validate.notNull(type,"类型不能为空");
        String description = bonusDto.getDescription();
        BigDecimal bonus = bonusDto.getBonus();
        if (null==bonus||bonus.compareTo(BigDecimal.ZERO)<0){
            throw new BaseException("金额不能为空或者要大于0");
        }
        JkBalanceEntity balance=jkBalanceMapper.getBalanceByEmpId(empNo);
        //领取
        if (type.equals(0)) {
            synchronized (this) {
                if (null==balance){
                    throw new BaseException("没有余额记录");
                }
                if (balance.getBalance().compareTo(bonus)<0){
                    throw new BaseException("余额不足");
                }
                JkRewardLogEntity entity = new JkRewardLogEntity();
                entity.setDescription(description);
                entity.setType(0);
                entity.setMoney(bonus);
                entity.setEmpId(empNo);
                entity.setCompanyId(companyNo);
                jkRewardLogMapper.insert(entity);
                return upBalanceOperation(bonusDto,balance);
            }
        }
        //发放奖金
        if (null == balance) {
            JkBalanceEntity balanceEntity = new JkBalanceEntity();
            balanceEntity.setEmpId(empNo);
            balanceEntity.setCompanyId(companyNo);
            balanceEntity.setBalance(bonus);
            balanceEntity.setCurIncrement(bonus);
            jkBalanceMapper.insertBalance(balanceEntity);
        } else {
            upBalanceOperation(bonusDto,balance);
        }
        JkRewardLogEntity entity = new JkRewardLogEntity();
        entity.setDescription(description);
        entity.setType(1);
        entity.setMoney(bonus);
        entity.setEmpId(empNo);
        entity.setCompanyId(companyNo);
        return jkRewardLogMapper.insert(entity);
    }

    /**
     * 更新余额操作
     * @param bonusDto
     * @param balance
     * @return
     */
    private int upBalanceOperation(BonusDto bonusDto,JkBalanceEntity balance) {
        JkBalanceEntity balanceEntity = new JkBalanceEntity();
        balanceEntity.setEmpId(bonusDto.getEmpNo());
        if (bonusDto.getType().equals(0)) {
            balanceEntity.setBalance(balance.getBalance().subtract(bonusDto.getBonus()));
        }
        if (bonusDto.getType().equals(1)) {
            balanceEntity.setBalance(balance.getBalance().add(bonusDto.getBonus()));
        }
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DATE, 1);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String firstDay = format.format(calendar.getTime());
        calendar.roll(Calendar.DATE, -1);
        String lastDay = format.format(calendar.getTime());

        BigDecimal money=jkRewardLogMapper.selectCount(firstDay,lastDay,bonusDto.getType());
        balanceEntity.setCurDecrement(money);
        return jkBalanceMapper.updateBalance(balanceEntity);
    }


    @Override
    public int deleteJkRewardLogById(String id) {
        Validate.notNull(id,"id不能为空");
        return jkRewardLogMapper.deleteById(id);
    }
}
