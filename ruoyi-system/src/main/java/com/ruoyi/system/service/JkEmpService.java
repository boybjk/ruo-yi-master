package com.ruoyi.system.service;

import com.ruoyi.system.domain.JkEmpEntity;

import java.util.Set;

public interface JkEmpService {

    /**
     * @description 根据id查询员工信息
     * @param id
     * @return
     */
    JkEmpEntity getJkEmpById(String id);

    /**
     * @description 获取所有员工信息
     * @return
     */
    Set<JkEmpEntity> getAllEmp();

    /**
     * @description 新增员工信息
     * @param jkEmpEntity
     * @return
     */
    int addJkEmp(JkEmpEntity jkEmpEntity);


}
