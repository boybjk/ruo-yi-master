package com.ruoyi.system.service;


import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.system.dto.BalanceDto;

import java.util.List;

/**
* @author boybjk
* @description 针对表【jk_balance(余额)】的数据库操作Service
* @createDate
*/
public interface JkBalanceService {

    /**
    * @description 分页查询
    * @param balanceDto 分页对象
    * @return TableDataInfo
    */
    List<BalanceDto> getJkBalanceList(BalanceDto balanceDto);


}
