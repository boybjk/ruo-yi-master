package com.ruoyi.system.service;

import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.system.dto.BonusDto;

import java.util.List;

public interface JkRewardLogService {

    /**
     * @description 分页查询
     * @param bonusPage 分页对象
     * @return TableDataInfo
     */
    List<BonusDto> getJkRewardLogResultPage(BonusDto bonusPage);

    /**
     * @description 新增奖金or取奖金记录
     * @param bonusDto 实体对象
     * @return int
     */
    int addJkRewardLog(BonusDto bonusDto);

    /**
     * @description 根据id删除奖金or取奖金记录
     * @param id
     * @return
     */
    int deleteJkRewardLogById(String id);
}
