package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.exception.base.BaseException;
import com.ruoyi.system.domain.JkBalanceEntity;
import com.ruoyi.system.domain.JkEmpEntity;
import com.ruoyi.system.dto.BalanceDto;
import com.ruoyi.system.mapper.JkBalanceMapper;
import com.ruoyi.system.mapper.JkCompanyMapper;
import com.ruoyi.system.mapper.JkEmpMapper;
import com.ruoyi.system.service.JkBalanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
* @author boybjk
* @description 针对表【jk_balance(余额)】的数据库操作Service实现
* @createDate
*/
@Service
public class JkBalanceServiceImpl implements JkBalanceService{

    @Autowired
    private JkBalanceMapper jkBalanceMapper;
    @Autowired
    private JkEmpMapper jkEmpMapper;
    @Autowired
    private JkCompanyMapper jkCompanyMapper;

    @Override
    public List<BalanceDto> getJkBalanceList(BalanceDto balancePage){
        List<JkBalanceEntity> JkBalancelist=jkBalanceMapper.selectJkRewardLogListPage(balancePage);
        if (null==JkBalancelist||JkBalancelist.size()==0){
            return new ArrayList<>();
        }
        List<BalanceDto> balanceList=new ArrayList<>();
        for (JkBalanceEntity entity : JkBalancelist) {
            BalanceDto balanceDto = new BalanceDto();
            String empId = entity.getEmpId();
            String companyId = entity.getCompanyId();
            JkEmpEntity jkEmpEntity = jkEmpMapper.selectById(empId);
            if (null==jkEmpEntity){
                throw new BaseException("未查询到员工信息");
            }
            String companyName=jkCompanyMapper.selectById(companyId);
            if (null==companyName||"".equals(companyName)){
                throw new BaseException("未查询到公司信息");
            }
            balanceDto.setEmpName(jkEmpEntity.getEmpName());
            balanceDto.setCompanyName(companyName);
            balanceDto.setDepName(jkEmpEntity.getDepName());
            balanceDto.setCurDecrement(entity.getCurDecrement());
            balanceDto.setCurIncrement(entity.getCurIncrement());
            balanceDto.setBalance(entity.getBalance());
            balanceDto.setCompanyId(entity.getCompanyId());
            balanceDto.setEmpId(entity.getEmpId());
            balanceList.add(balanceDto);
        }
        return balanceList;
    }
}




