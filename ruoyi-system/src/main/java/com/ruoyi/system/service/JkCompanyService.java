package com.ruoyi.system.service;

import com.ruoyi.system.domain.JkCompanyEntity;
import com.ruoyi.system.domain.JkEmpEntity;

import java.util.Set;

public interface JkCompanyService {

    /**
     * @description 获取所有公司信息
     * @return
     */
    Set<JkCompanyEntity> getAllCompany();

    /**
     * @description 新增公司信息
     * @param jkCompanyEntity
     * @return
     */
    int addJkCompany(JkCompanyEntity jkCompanyEntity);

    /**
     * @description 根据公司名称查询公司信息
     * @param name
     * @return
     */
    JkCompanyEntity getJkEmpByName(String name);
}
