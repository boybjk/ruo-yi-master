package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.uuid.UUID;
import com.ruoyi.system.domain.JkEmpEntity;
import com.ruoyi.system.mapper.JkEmpMapper;
import com.ruoyi.system.service.JkEmpService;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;


@Service
public class JkempServiceImpl implements JkEmpService {

    @Autowired
    private JkEmpMapper jkEmpMapper;

    @Override
    public JkEmpEntity getJkEmpById(String id) {
        Validate.notNull(id,"id不能为空");
        return jkEmpMapper.selectById(id);
    }


    @Override
    public Set<JkEmpEntity> getAllEmp() {
        return jkEmpMapper.selectAll();
    }

    @Override
    public int addJkEmp(JkEmpEntity jkEmpEntity) {
        Validate.notNull(jkEmpEntity.getDepName(),"DepName不能为空");
        Validate.notNull(jkEmpEntity.getEmpName(),"empName不能为空");
        Validate.notNull(jkEmpEntity.getCompanyId(),"companyId不能为空");
        jkEmpEntity.setId(UUID.randomUUID().toString());
        return jkEmpMapper.insert(jkEmpEntity);
    }


}
