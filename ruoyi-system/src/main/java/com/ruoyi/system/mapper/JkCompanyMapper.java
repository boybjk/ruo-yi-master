package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.JkCompanyEntity;
import com.ruoyi.system.domain.JkEmpEntity;

import java.util.Set;

/**
* @author boybjk
* @description 针对表【jk_company(公司表)】的数据库操作Mapper
* @Entity com.ruoyi.system.domain.JkCompanyEntity
*/
public interface JkCompanyMapper {

    int deleteByPrimaryKey(Long id);

    int insert(JkCompanyEntity record);

    int insertSelective(JkCompanyEntity record);

    JkCompanyEntity selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(JkCompanyEntity record);

    int updateByPrimaryKey(JkCompanyEntity record);

    /**
     * 根据公司id查询公司名称
     * @param companyId
     * @return
     */
    String selectById(String companyId);
    /**
     * 查询所有公司信息
     * @return
     */
    Set<JkCompanyEntity> selectAll();

    /**
     * 根据公司名称查询公司信息
     * @param name
     * @return
     */
    JkCompanyEntity selectByName(String name);
}
