package com.ruoyi.system.mapper;

import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.system.domain.JkRewardLogEntity;
import com.ruoyi.system.dto.BonusDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
* @author boybjk
* @description 针对表【jk_reward_log(奖金操作日志)】的数据库操作Mapper
* @Entity com.ruoyi.system.domain.JkRewardLogEntity
*/
public interface JkRewardLogMapper {

    int deleteByPrimaryKey(Long id);

    int insert(JkRewardLogEntity record);

    int insertSelective(JkRewardLogEntity record);

    JkRewardLogEntity selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(JkRewardLogEntity record);

    int updateByPrimaryKey(JkRewardLogEntity record);

    /**
     * @description 分页查询
     * @param bonusPage 分页对象
     * @return List<JkRewardLogEntity>
     */
    List<JkRewardLogEntity> selectJkRewardLogListPage(BonusDto bonusPage);

    /**
     * 获取总记录数
     * @return
     */
    long getTotal();

    /**
     * 当月总领取余额
     * @param firstDay
     * @param lastDay
     * @param type 0:领取 1:发放
     * @return
     */
    BigDecimal selectCount(@Param("firstDay") String firstDay,
                           @Param("lastDay") String lastDay,
                           @Param("type") Integer type);

    /**
     * 根据id删除记录
     * @param id
     * @return
     */
    int deleteById(String id);
}
