package com.ruoyi.system.mapper;

import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.system.domain.JkBalanceEntity;
import com.ruoyi.system.domain.JkRewardLogEntity;
import com.ruoyi.system.dto.BalanceDto;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
* @author boybjk
* @description 针对表【jk_balance(余额)】的数据库操作Mapper
* @Entity com.ruoyi.system.domain.JkBalanceEntity
*/
public interface JkBalanceMapper {

    /**
     * 根据员工id查询余额
     * @param empNo
     * @return
     */
    JkBalanceEntity getBalanceByEmpId(String empNo);

    /**
     * 根据员工id更新余额
     * @param empNo
     * @param balance
     * @return
     */
    int updateBalanceByEmpId(@Param("empNo") String empNo, @Param("balance") BigDecimal balance);

    /**
     * 更新余额
     * @param entity
     * @return
     */
    int updateBalance(JkBalanceEntity entity);

    /**
     *
     * @param balanceEntity
     */
    void insertBalance(JkBalanceEntity balanceEntity);

    /**
     * @description 分页查询
     * @param balanceDto
     * @return
     */
    List<JkBalanceEntity> selectJkRewardLogListPage(BalanceDto balanceDto);

    /**
     * @description 查询总数
     * @return
     */
    long getTotal();
}




