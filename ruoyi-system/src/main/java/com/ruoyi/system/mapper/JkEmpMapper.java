package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.JkEmpEntity;

import java.util.Set;

/**
* @author boybjk
* @description 针对表【jk_emp(员工表)】的数据库操作Mapper
* @Entity com.ruoyi.system.domain.JkEmpEntity
*/
public interface JkEmpMapper {

    int deleteByPrimaryKey(Long id);

    int insert(JkEmpEntity record);

    int insertSelective(JkEmpEntity record);

    int updateByPrimaryKeySelective(JkEmpEntity record);

    int updateByPrimaryKey(JkEmpEntity record);

    /**
     * 根据员工id查询员工
     * @param empId
     * @return
     */
    JkEmpEntity selectById(String empId);
    /**
     * 查询所有员工信息
     * @return
     */
    Set<JkEmpEntity> selectAll();

}
