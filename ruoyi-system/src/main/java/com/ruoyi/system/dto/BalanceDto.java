package com.ruoyi.system.dto;

import com.ruoyi.common.core.domain.BaseEntity;

import java.io.Serializable;
import java.math.BigDecimal;

public class BalanceDto extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private String id;
    private String empName;
    private String depName;
    private String companyName;
    private String companyId;
    private String empId;
    /**
     * 本月领取
     */
    private BigDecimal curDecrement;

    /**
     * 本月新增
     */
    private BigDecimal curIncrement;
    /**
     * 剩余奖金
     */
    private BigDecimal balance;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getDepName() {
        return depName;
    }

    public void setDepName(String depName) {
        this.depName = depName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public BigDecimal getCurDecrement() {
        return curDecrement;
    }

    public void setCurDecrement(BigDecimal curDecrement) {
        this.curDecrement = curDecrement;
    }

    public BigDecimal getCurIncrement() {
        return curIncrement;
    }

    public void setCurIncrement(BigDecimal curIncrement) {
        this.curIncrement = curIncrement;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }
}
