package com.ruoyi.system.dto;

import com.ruoyi.common.core.domain.BaseEntity;

import java.io.Serializable;
import java.math.BigDecimal;

public class BonusDto extends BaseEntity {
    private static final long serialVersionUID = 1L;

    private String id;
    private String  company;
    private String  companyNo;
    private String empName;
    private String empNo;
    private Integer type;
    private BigDecimal bonus;
    private String description ;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getCompanyNo() {
        return companyNo;
    }

    public void setCompanyNo(String companyNo) {
        this.companyNo = companyNo;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getEmpNo() {
        return empNo;
    }

    public void setEmpNo(String empNo) {
        this.empNo = empNo;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public BigDecimal getBonus() {
        return bonus;
    }

    public void setBonus(BigDecimal bonus) {
        this.bonus = bonus;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "BonusDto{" +
                "id='" + id + '\'' +
                ", company='" + company + '\'' +
                ", companyNo='" + companyNo + '\'' +
                ", empName='" + empName + '\'' +
                ", empNo='" + empNo + '\'' +
                ", type=" + type +
                ", bonus=" + bonus +
                ", description='" + description + '\'' +
                '}';
    }
}
