package com.ruoyi.system.domain;

import java.io.Serializable;

/**
 * 员工表
 * @TableName jk_emp
 */
public class JkEmpEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private String id;

    /**
     * 公司id
     */
    private String companyId;

    /**
     * 部门名称
     */
    private String depName;

    /**
     * 员工名
     */
    private String empName;


    /**
     * ID
     */
    public String getId() {
        return id;
    }

    /**
     * ID
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 公司id
     */
    public String getCompanyId() {
        return companyId;
    }

    /**
     * 公司id
     */
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    /**
     * 部门名称
     */
    public String getDepName() {
        return depName;
    }

    /**
     * 部门名称
     */
    public void setDepName(String depName) {
        this.depName = depName;
    }

    /**
     * 员工名
     */
    public String getEmpName() {
        return empName;
    }

    /**
     * 员工名
     */
    public void setEmpName(String empName) {
        this.empName = empName;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        JkEmpEntity other = (JkEmpEntity) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getCompanyId() == null ? other.getCompanyId() == null : this.getCompanyId().equals(other.getCompanyId()))
            && (this.getDepName() == null ? other.getDepName() == null : this.getDepName().equals(other.getDepName()))
            && (this.getEmpName() == null ? other.getEmpName() == null : this.getEmpName().equals(other.getEmpName()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getCompanyId() == null) ? 0 : getCompanyId().hashCode());
        result = prime * result + ((getDepName() == null) ? 0 : getDepName().hashCode());
        result = prime * result + ((getEmpName() == null) ? 0 : getEmpName().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", companyId=").append(companyId);
        sb.append(", depName=").append(depName);
        sb.append(", empName=").append(empName);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}