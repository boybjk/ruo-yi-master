package com.ruoyi.system.domain;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 奖金操作日志
 * @TableName jk_reward_log
 */
public class JkRewardLogEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private String id;

    /**
     * 公司id
     */
    private String companyId;

    /**
     * 员工id
     */
    private String empId;

    /**
     * 默认为0-提取  1-添加
     */
    private Integer type;

    /**
     * 金额
     */
    private BigDecimal money;

    /**
     * 描述
     */
    private String description;

    private String createBy;
    private String createTime;
    private String updateBy;
    private String updateTime;

    private Integer deleted;

    /**
     * ID
     */
    public String getId() {
        return id;
    }

    /**
     * ID
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 公司id
     */
    public String getCompanyId() {
        return companyId;
    }

    /**
     * 公司id
     */
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    /**
     * 员工id
     */
    public String getEmpId() {
        return empId;
    }

    /**
     * 员工id
     */
    public void setEmpId(String empId) {
        this.empId = empId;
    }


    /**
     * 默认为0-提取  1-添加
     */
    public Integer getType() {
        return type;
    }

    /**
     * 默认为0-提取  1-添加
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 金额
     */
    public BigDecimal getMoney() {
        return money;
    }

    /**
     * 金额
     */
    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    /**
     * 描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * 描述
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreate_by() {
        return createBy;
    }

    public void setCreate_by(String create_by) {
        this.createBy = create_by;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
}