package com.ruoyi.system.domain;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 余额
 * @TableName jk_balance
 */
public class JkBalanceEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private String id;

    /**
     * 公司id
     */
    private String companyId;

    /**
     * 员工id
     */
    private String empId;

    /**
     * 本月领取
     */
    private BigDecimal curDecrement;

    /**
     * 本月新增
     */
    private BigDecimal curIncrement;

    /**
     * 剩余奖金
     */
    private BigDecimal balance;

    private String createBy;
    private String createTime;
    private String updateBy;
    private String updateTime;



    /**
     * ID
     */
    public String getId() {
        return id;
    }

    /**
     * ID
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 公司id
     */
    public String getCompanyId() {
        return companyId;
    }

    /**
     * 公司id
     */
    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    /**
     * 员工id
     */
    public String getEmpId() {
        return empId;
    }

    /**
     * 员工id
     */
    public void setEmpId(String empId) {
        this.empId = empId;
    }

    /**
     * 本月领取
     */
    public BigDecimal getCurDecrement() {
        return curDecrement;
    }

    /**
     * 本月领取
     */
    public void setCurDecrement(BigDecimal curDcrement) {
        this.curDecrement = curDcrement;
    }

    /**
     * 本月新增
     */
    public BigDecimal getCurIncrement() {
        return curIncrement;
    }

    /**
     * 本月新增
     */
    public void setCurIncrement(BigDecimal curIncrment) {
        this.curIncrement = curIncrment;
    }

    /**
     * 剩余奖金
     */
    public BigDecimal getBalance() {
        return balance;
    }

    /**
     * 剩余奖金
     */
    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getCreate_by() {
        return createBy;
    }

    public void setCreate_by(String create_by) {
        this.createBy = create_by;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public String getUpdate_time() {
        return updateTime;
    }

    public void setUpdate_time(String update_time) {
        this.updateTime = update_time;
    }
}